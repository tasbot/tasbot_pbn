use crate::chat_client::ChatClient;
use anyhow::anyhow;
use irc_proto::Message;
use lockfree::channel::mpsc::Sender;
use parity_ws as ws;
use std::sync::{atomic::AtomicBool, Arc, Mutex};

pub const TWITCH_CHAT_WS_URL: &str = "wss://irc-ws.chat.twitch.tv";

const PING: ws::util::Token = ws::util::Token(0);
const EXPIRE: ws::util::Token = ws::util::Token(1);
const CLOSE: ws::util::Token = ws::util::Token(2);

const PING_TIMEOUT_MS: u64 = 15_000;
const EXPIRE_TIMEOUT_MS: u64 = 60_000;
const CLOSE_TIMEOUT_MS: u64 = 5_000;

#[derive(Debug, Clone)]
pub struct TwitchWsClient {
    out: Option<ws::Sender>,

    tx: Sender<crate::TaggedMessage>,
    username: String,
    password: String,
    channels: Vec<String>,

    timeout_ping: Option<ws::util::Timeout>,
    timeout_expire: Option<ws::util::Timeout>,
    timeout_close: Option<ws::util::Timeout>,

    shutdown: Arc<AtomicBool>,
    stop: Arc<AtomicBool>,

    pub retry: Arc<Mutex<u8>>,
}

/// Wrapper handling a Twitch Chat WebSocket connection
impl TwitchWsClient {
    pub fn new(
        tx: Sender<crate::TaggedMessage>,
        stop: Arc<AtomicBool>,
        channels: &[String],
        username: &Option<String>,
        password: &Option<String>,
    ) -> Self {
        // Use the supplied user information and default to Twitch Chat guest credentials for read-only chat
        let username = username.clone().unwrap_or_else(TwitchWsClient::justinfan);
        let password = password.clone().unwrap_or_else(|| "justinfan".to_owned());

        Self {
            out: None,
            tx,
            username,
            password,
            channels: channels.to_owned(),

            retry: Arc::new(Mutex::new(0)),

            timeout_ping: None,
            timeout_expire: None,
            timeout_close: None,

            shutdown: Arc::new(AtomicBool::new(false)),
            stop,
        }
    }

    /// Generate a Twitch Chat guest name
    pub fn justinfan() -> String {
        format!(
            "justinfan{}",
            std::time::SystemTime::now()
                .duration_since(std::time::UNIX_EPOCH)
                .unwrap()
                .as_secs()
        )
    }
}

#[async_trait::async_trait]
impl ChatClient for TwitchWsClient {
    fn get_name(&self) -> &'static str {
        "TwitchWS"
    }

    fn get_retry_count(&self) -> u8 {
        *self.retry.lock().unwrap()
    }

    fn get_retry(&mut self) -> Arc<Mutex<u8>> {
        self.retry.clone()
    }

    /// Connect to the Twitch Chat WebSocket IRC server
    async fn connect(&mut self) -> crate::Result<()> {
        ws::connect(TWITCH_CHAT_WS_URL, |out| {
            let out2 = out.clone();
            let stop = Arc::clone(&self.stop);
            let shutdown = Arc::clone(&self.shutdown);

            std::thread::spawn(move || loop {
                if stop.load(std::sync::atomic::Ordering::SeqCst)
                    || shutdown.load(std::sync::atomic::Ordering::SeqCst)
                {
                    let _ = out2.shutdown();
                }
                std::thread::sleep(std::time::Duration::from_millis(100));
            });

            self.out = Some(out);
            self.clone()
        })
        .map_err(Into::into)
    }
}

/// See: https://docs.rs/ws/0.9.1/ws/trait.Handler.html
impl ws::Handler for TwitchWsClient {
    /// Called when a request to shutdown all connections has been received.
    fn on_shutdown(&mut self) {
        self.shutdown
            .store(true, std::sync::atomic::Ordering::SeqCst);
    }

    /// Called when the WebSocket handshake is successful and the connection is open for sending and receiving messages.
    fn on_open(&mut self, _: ws::Handshake) -> ws::Result<()> {
        log::info!("Connected to WebSocket");

        // Schedule timeouts
        self.out.as_ref().unwrap().timeout(PING_TIMEOUT_MS, PING)?;
        self.out
            .as_ref()
            .unwrap()
            .timeout(EXPIRE_TIMEOUT_MS, EXPIRE)?;

        // Identify to Twitch Chat IRC
        self.out
            .as_ref()
            .unwrap()
            .send("CAP REQ :twitch.tv/tags twitch.tv/commands twitch.tv/membership")
            .and_then(|_| {
                self.out
                    .as_ref()
                    .unwrap()
                    .send(format!("PASS {}", self.password))
            })
            .and_then(|_| {
                self.out
                    .as_ref()
                    .unwrap()
                    .send(format!("NICK {}", self.username))
            })
            .and_then(|_| {
                self.out
                    .as_ref()
                    .unwrap()
                    .send(format!("USER {} 8 * :{}", self.username, self.username))
            })
            .and_then(|_| {
                self.out
                    .as_ref()
                    .unwrap()
                    .send(format!("JOIN {}", self.channels.join(",")))
            })
    }

    /// Called when an error occurs on the WebSocket.
    fn on_error(&mut self, err: ws::Error) {
        if let Some(t) = self.timeout_ping.take() {
            self.out.as_ref().unwrap().cancel(t).unwrap();
        }
        if let Some(t) = self.timeout_expire.take() {
            self.out.as_ref().unwrap().cancel(t).unwrap();
        }
        if let Some(t) = self.timeout_close.take() {
            self.out.as_ref().unwrap().cancel(t).unwrap();
        }

        log::error!("{:?}", err);
    }

    /// Called any time this endpoint receives a close control frame.
    /// This may be because the other endpoint is initiating a closing handshake,
    /// or it may be the other endpoint confirming the handshake initiated by this endpoint.
    fn on_close(&mut self, code: ws::CloseCode, reason: &str) {
        if let Some(t) = self.timeout_ping.take() {
            self.out.as_ref().unwrap().cancel(t).unwrap();
        }
        if let Some(t) = self.timeout_expire.take() {
            self.out.as_ref().unwrap().cancel(t).unwrap();
        }
        if let Some(t) = self.timeout_close.take() {
            self.out.as_ref().unwrap().cancel(t).unwrap();
        }

        log::info!("Connection closing due to ({:?}) {}", code, reason);
    }

    /// Called on incoming messages.
    fn on_message(&mut self, msg: ws::Message) -> ws::Result<()> {
        let text = msg.as_text().unwrap();

        for message in text.split("\r\n") {
            if message.trim().is_empty() {
                continue;
            }

            let message = Message::from(message);
            let mut own = false;

            if let Some(nickname) = message.source_nickname() {
                own = self.username == nickname;
            }

            match &message.command {
                irc_proto::Command::PING(ref data, _) => {
                    log::debug!("Received IRC PING: {:?}", data);

                    let pong = &irc_proto::Command::PONG(data.to_owned(), None);
                    let pong: String = pong.into();

                    log::debug!("Sending IRC PONG: {:?}", pong);

                    self.out.as_ref().unwrap().send(ws::Message::Text(pong))?;
                }
                irc_proto::Command::PART(channel, _) => {
                    if own && self.channels.contains(channel) {
                        // Somehow we left a configured channel, error out
                        return Err(ws::Error::new(
                            ws::ErrorKind::Io(std::io::Error::new(
                                std::io::ErrorKind::Other,
                                anyhow!("Left configured channel {}", channel),
                            )),
                            "parted_channel",
                        ));
                    }
                }
                irc_proto::Command::JOIN(channel, _, _) => {
                    if own && self.channels.contains(channel) {
                        log::info!("Joined {}", channel);

                        // Reset retry counter on successful join
                        *self.retry.lock().unwrap() = 0;
                    }
                }
                _ => (),
            }

            self.tx.send(("[WS]", message)).unwrap();
        }

        Ok(())
    }

    /// Called when a timeout is triggered.
    ///
    /// This method will be called when the eventloop encounters a timeout on the specified
    /// token. To schedule a timeout with your specific token use the `Sender::timeout` method.
    fn on_timeout(&mut self, event: ws::util::Token) -> ws::Result<()> {
        log::trace!("Timeout: {:?}", &event);

        match event {
            PING => {
                log::debug!("Sending WebSocket PING");

                // Update the ping timer
                self.out.as_ref().unwrap().timeout(PING_TIMEOUT_MS, PING)?;

                // Send a PING frame
                self.out.as_ref().unwrap().ping(vec![])
            }
            EXPIRE => {
                // We haven’t received any frames in the last `CLOSE_TIMEOUT_MS` milliseconds.
                log::info!("WebSocket ping timeout");

                // Try to close the connection properly.
                let result = self.out.as_ref().unwrap().close(ws::CloseCode::Error);
                // Schedule an abort timer for our CLOSE frame
                self.out
                    .as_ref()
                    .unwrap()
                    .timeout(CLOSE_TIMEOUT_MS, CLOSE)?;

                result
            }
            CLOSE => {
                // Our CLOSE frame hasn’t been answered, the connection is broken.
                log::info!("Close timeout!");

                Err(ws::Error::new(
                    ws::ErrorKind::Io(std::io::Error::new(
                        std::io::ErrorKind::TimedOut,
                        anyhow!("timeout"),
                    )),
                    "timeout",
                ))
            }
            _ => Err(ws::Error::new(
                ws::ErrorKind::Internal,
                "Invalid timeout token encountered!",
            )),
        }
    }

    /// Called when a timeout has been scheduled on the eventloop.
    ///
    /// This method is the hook for obtaining a Timeout object that may be used to cancel a
    /// timeout. This is a noop by default.
    fn on_new_timeout(
        &mut self,
        event: ws::util::Token,
        timeout: ws::util::Timeout,
    ) -> ws::Result<()> {
        log::trace!("New timeout: {:?}; {:?}", &event, &timeout);

        // Cancel previous timeouts and replace them with the new one
        match event {
            PING => {
                if let Some(t) = self.timeout_ping.take() {
                    self.out.as_ref().unwrap().cancel(t)?;
                }
                self.timeout_ping = Some(timeout);
            }
            EXPIRE => {
                if let Some(t) = self.timeout_expire.take() {
                    self.out.as_ref().unwrap().cancel(t)?;
                }
                self.timeout_expire = Some(timeout);
            }
            CLOSE => {
                if let Some(t) = self.timeout_close.take() {
                    self.out.as_ref().unwrap().cancel(t)?;
                }
                self.timeout_close = Some(timeout);
            }
            _ => {
                return Err(ws::Error::new(
                    ws::ErrorKind::Internal,
                    "Invalid timeout token encountered!",
                ));
            }
        }

        Ok(())
    }

    /// A method for handling incoming frames.
    ///
    /// This method provides very low-level access to the details of the WebSocket protocol. It may
    /// be necessary to implement this method in order to provide a particular extension, but
    /// incorrect implementation may cause the other endpoint to fail the connection.
    ///
    /// Returning `Ok(None)` will cause the connection to forget about a particular frame. This is
    /// useful if you want ot filter out a frame or if you don't want any of the default handler
    /// methods to run.
    ///
    /// By default this method simply ensures that no reserved bits are set.
    fn on_frame(&mut self, frame: ws::Frame) -> ws::Result<Option<ws::Frame>> {
        // We received a frame, update our ping and expiry timeouts
        self.out.as_ref().unwrap().timeout(PING_TIMEOUT_MS, PING)?;
        self.out
            .as_ref()
            .unwrap()
            .timeout(EXPIRE_TIMEOUT_MS, EXPIRE)?;

        // Dumb struct used to get the default trait implementation.
        // In “ws” version 0.9.1 this function does not use the `self` argument,
        // so this should be safe.
        struct DefaultHandler;
        impl ws::Handler for DefaultHandler {}
        DefaultHandler.on_frame(frame)
    }
}
