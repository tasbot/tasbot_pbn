use cursive::{Printer, Vec2, View};
use parking_lot::RwLock;
use ringbuffer::{AllocRingBuffer, RingBuffer, RingBufferExt};
use std::sync::Arc;

const DEFAULT_MESSAGE: &str = "None so far";

pub struct MessageHistory {
    history: Arc<RwLock<AllocRingBuffer<String>>>,
}

impl MessageHistory {
    pub fn new(history: Arc<RwLock<AllocRingBuffer<String>>>) -> Self {
        Self { history }
    }
}

impl View for MessageHistory {
    fn required_size(&mut self, _constraint: Vec2) -> Vec2 {
        let history = self.history.read();
        let height = history.len();

        if history.is_empty() {
            return Vec2::new(DEFAULT_MESSAGE.chars().count() + 4, 1);
        }

        let width = history.iter().map(|m| m.chars().count()).max().unwrap_or(0);
        Vec2::new(width, height)
    }

    fn draw(&self, printer: &Printer<'_, '_>) {
        let history = self.history.read();

        if history.is_empty() {
            printer.print((2, 0), DEFAULT_MESSAGE);
            return;
        }

        let skipped = history.len().saturating_sub(printer.size.y);

        for (i, message) in history.iter().skip(skipped).enumerate() {
            printer.print((0, i), message);
        }
    }
}
